# python jsonToGPX.py > loraTracking.gpx
import json

path = 'loraRangeTestData2.txt'

print('<?xml version="1.0"?>\n<gpx version="1.1">')
with open(path) as f:
    lines = f.readlines()
    for line in lines:
        message = json.loads(line.replace("test013525325230975231762196298707/devices/lora01/up ", ""))
        time = message['metadata']['gateways'][0]['time']
        rssi = message['metadata']['gateways'][0]['rssi']
        latitude = message['payload_fields']['lat']
        longitude = message['payload_fields']['lng']
        gtw_id = message['metadata']['gateways'][0]['gtw_id']
        snr = message['metadata']['gateways'][0]['snr']
        data_rate = message['metadata']['data_rate']
        counter = message['counter']

        print("    <wpt lat=\"" + str(latitude) + "\" lon=\"" + str(longitude) + "\">\n\
           <time>" + time + "</time>\n\
           <name>" + data_rate + " snr" + str(snr) +" "+ str(rssi) + "dBm - cnt" + str(counter) + " - gw:" + str(gtw_id) + "</name>\n</wpt>\n")
print ("    </gpx>")
