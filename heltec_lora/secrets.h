#define OTAA_MODE = 1 // make this line a commment to switch to ABP mode

#ifdef OTAA_MODE
const char* loraMode = "OTAA";
// ### OTAA (Over-the-air activation) ###
// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
static const u1_t PROGMEM APPEUI[8]={  };

// This should also be in little endian format, see above.
static const u1_t PROGMEM DEVEUI[8]={  };

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
static const u1_t PROGMEM APPKEY[16] = {  };

#else

// ### ABP (Activation-by-personalisation) ##
String loraMode = "ABP";
// LoRaWAN NwkSKey, network session key
static const PROGMEM u1_t NWKSKEY[16] = {  };

// LoRaWAN AppSKey, application session key
static const u1_t PROGMEM APPSKEY[16] = {  };

// LoRaWAN end-device address (DevAddr)
// See http://thethingsnetwork.org/wiki/AddressSpace
// The library converts the address to network byte order as needed.
static const u4_t DEVADDR = 0x ; // <-- Change this address for every node!

#endif

// not lora specific values
const char* ssid01     = "SSID";
const char* password01 = "PASSWORD";

const char* ssid02     = "SSID";
const char* password02 = "PASSWORD";
