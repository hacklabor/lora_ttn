/*******************************************************************************
 * Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman
 *
 * Permission is hereby granted, free of charge, to anyone
 * obtaining a copy of this document and accompanying files,
 * to do whatever they want with them without any restriction,
 * including, but not limited to, copying, modification and redistribution.
 * NO WARRANTY OF ANY KIND IS PROVIDED.
 *
 * This example sends a valid LoRaWAN packet with payload "Hello,
 * world!", using frequency and encryption settings matching those of
 * the The Things Network.
 *
 * This uses ABP (Activation-by-personalisation), where a DevAddr and
 * Session keys are preconfigured (unlike OTAA, where a DevEUI and
 * application key is configured, while the DevAddr and session keys are
 * assigned/generated in the over-the-air-activation procedure).
 *
 * Note: LoRaWAN per sub-band duty-cycle limitation is enforced (1% in
 * g1, 0.1% in g2), but not the TTN fair usage policy (which is probably
 * violated by this sketch when left running for longer)!
 *
 * To use this sketch, first register your application and device with
 * the things network, to set or generate a DevAddr, NwkSKey and
 * AppSKey. Each device should have their own unique values for these
 * fields.
 *
 * Do not forget to define the radio type correctly in config.h.
 *
 *******************************************************************************/
#include <Ucglib.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <XPT2046_Touchscreen.h>
#include <string>
#include <PubSubClient.h>
#include <WiFi.h>
#include <ETH.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <FuGPS.h>
#include <secrets.h> 
WiFiClient espClient;
HardwareSerial Serial5(2);
FuGPS fuGPS(Serial5);
PubSubClient client(espClient);
String mqttvalue[60];
String alt_MQTT_TIME;
static osjob_t sendjob;
ostime_t alt_TX = 0;
ostime_t diff = 0;
ostime_t diffMS = 0;
byte cue = 0;
byte NextTX = 0;
byte AutoTX = 0;
byte StatusMqtt =2;
extern u1_t developer1 = 0;
unsigned long DelayTime[] = {134000,75000,35000,20000,10000,5000};
unsigned long display_alt = 0;
unsigned long mqtt_alt = 0;
unsigned long gps_alt = 0;
unsigned long Time2TX = 0;
unsigned long NextSendTime = 0;
String DisplayText[50];
String DisplayTextOld[50];

byte M_FIX=99;
byte M_nextTX = 99;
byte M_Mqtt = 99;



#pragma region Display


#define TFT_CS 21  //for D1 mini or TFT I2C Connector Shield (V1.1.0 or later)
#define TFT_DC 22  //for D1 mini or TFT I2C Connector Shield (V1.1.0 or later)
#define TFT_RST 12 //for D1 mini or TFT I2C Connector Shield (V1.1.0 or later)
#define TS_CS 13  //for D1 mini or TFT I2C Connector Shield (V1.1.0 or later)

//Ucglib_ILI9341_18x240x320_HWSPI ucg(/*cd=*/ TFT_DC, /*cs=*/ TFT_CS, /*reset=*/ TFT_RST);
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_RST);
XPT2046_Touchscreen ts(TS_CS);

const unsigned TX_INTERVAL = 3600;
dr_t SFx = 5;
dr_t SFxLast = 5;

double toRadians(double degree) {
	return degree * M_PI / 180;
}
void(*resetFunc) (void) = 0;//declare reset function at address 0

double getDistance(double lat1, double lon1, double lat2, double lon2) {
	double r = 6378137.0;   // earth radius in meter

	lat1 = toRadians(lat1);
	lon1 = toRadians(lon1);
	lat2 = toRadians(lat2);
	lon2 = toRadians(lon2);

	double dlat = lat2 - lat1;
	double dlon = lon2 - lon1;

	double d = 2 * r * asin(sqrt(pow(sin(dlat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlon / 2), 2)));

	return d;
}

void TFT_DrawText(String text,int16_t x, int16_t y, uint8_t size, uint16_t FrontColor, uint16_t BackColor)
{
	tft.setCursor(x, y);
	tft.setTextSize(size);
	tft.setTextColor(FrontColor, BackColor);
	tft.print(text);
}
void Display()
{ 
	
	if ((millis() - display_alt) > 200)
	{
		char buf[10];
		if (millis() > NextSendTime)
		{
			NextTX = 1;
		}
		else
		{
			NextTX = 0;
		}
		uint16_t GPSFrontColor;
		uint16_t GPSBackColor;
		tft.setTextWrap(false);
		display_alt = millis();
		if (fuGPS.hasFix() == true) {
			GPSFrontColor = ILI9341_BLACK;
			GPSBackColor = ILI9341_GREEN;
			if (M_FIX != 0)
			{
				tft.fillRect(0, 0, 320, 100, GPSBackColor);
				M_FIX = 0;
			}
			TFT_DrawText("  Dst: " + String(getDistance(53.601440, 11.418432, fuGPS.Latitude, fuGPS.Longitude)) + "m", 0, 18 * 3+7, 3, GPSFrontColor, GPSBackColor);
		}
		else
		{
			GPSFrontColor = ILI9341_WHITE;
			GPSBackColor = ILI9341_RED;
			if (M_FIX != 1)
			{
				tft.fillRect(0,0, 320, 100, GPSBackColor);
				M_FIX = 1;
			}
			TFT_DrawText(" NO FIX: "+ (String((millis() - gps_alt) / 1000) + " s"), 0, 18 * 3+7, 3, GPSFrontColor, GPSBackColor);
		}
		
		TFT_DrawText(" GPS DATA A: " + String(fuGPS.Accuracy) + " S: " + String(fuGPS.Satellites), 0, 5, 2, GPSFrontColor, GPSBackColor);
		TFT_DrawText(" Lat:" + String(fuGPS.Latitude, 4) + " Lng:" + String(fuGPS.Longitude, 4), 0, 18 * 2, 2, GPSFrontColor, GPSBackColor);
		
		//Mqtt
		uint16_t MqttFrontColor = ILI9341_WHITE;
		uint16_t MqttBackColor = ILI9341_BLACK;
		int offsetY = 112;
		if (StatusMqtt == 0)
		{
			MqttFrontColor = ILI9341_WHITE;
			MqttBackColor = ILI9341_ORANGE ;
			if (M_Mqtt != 0)
			{
				M_Mqtt = 0;
				tft.fillRect(0, 100, 320, 80, MqttBackColor);
				

			}
			TFT_DrawText("  Packet queued", 0, 120, 3, MqttFrontColor, MqttBackColor);
		}

		if (StatusMqtt == 1)
		{
			MqttFrontColor = ILI9341_BLACK;
			MqttBackColor = ILI9341_GREEN;
			if (M_Mqtt != 1)
			{
				M_Mqtt = 1;
				tft.fillRect(0, 100, 320, 80, MqttBackColor);
				

			}
			TFT_DrawText(mqttvalue[16].substring(16, 24) + " Ch:" + mqttvalue[17].substring(8, 9) + " " + mqttvalue[18], 15, 18 * 0 + offsetY, 2, MqttFrontColor, MqttBackColor);
			TFT_DrawText(mqttvalue[20] , 15, 18 * 1 + offsetY, 2, MqttFrontColor, MqttBackColor);
			TFT_DrawText(mqttvalue[11] + " " + String((millis() - mqtt_alt) / 1000) + " s                 ", 15, 18 * 2 + offsetY, 2, MqttFrontColor, MqttBackColor);
			if (AutoTX==1)
			{ 
			AutoTX = 0;
			SFx = 5;
			}
		}

		if (StatusMqtt == 2)
		{
			MqttFrontColor = ILI9341_WHITE;
			MqttBackColor = ILI9341_RED;
			if (M_Mqtt != 2)
			{
				M_Mqtt = 2;
				tft.fillRect(0, 100, 320, 80, MqttBackColor);
				
			}
			TFT_DrawText("     NO DATA", 0, 18 * 1 + offsetY, 3, MqttFrontColor, MqttBackColor);
			
		
			NextTX = 1;

			if (AutoTX == 1)
			{ 
				delay(5000);
				
				
				if (SFx == 0)
				{
					AutoTX = 0;
					SFx = 5;
				}
				else
				{
					NextSendTime = millis();
					SFx--;
					StatusMqtt == 0;
					delay(1000);
					do_send(&sendjob);
				}

			}
		}

		

		
		
		
		
		
		uint16_t NextTXFrontColor = ILI9341_WHITE;
		uint16_t NextTXBackColor = ILI9341_BLACK;
		
		if (NextTX == 1)
		{

			NextTXFrontColor = ILI9341_BLACK;
			 NextTXBackColor = ILI9341_GREEN;
			if (M_nextTX != 1)
			{
				M_nextTX = 1;
				tft.fillRect(0, 180, 320, 240-180, NextTXBackColor);
		}
			TFT_DrawText("ONE", 5, 240 - 40, 3, NextTXFrontColor, NextTXBackColor);
			TFT_DrawText("AUTO", 245, 240 - 40, 3, NextTXFrontColor, NextTXBackColor);
			
		}
		
		if (NextTX == 0)
		{

			NextTXFrontColor = ILI9341_WHITE;
			NextTXBackColor = ILI9341_RED;
			if (M_nextTX != 0)
			{
				M_nextTX = 0;
				tft.fillRect(0, 180, 320, 240 - 180, NextTXBackColor);
			}
			TFT_DrawText( String((NextSendTime-    millis()) / 1000) + " s   ", 0, 240-30, 3, NextTXFrontColor, NextTXBackColor);
			
		}

		tft.drawRect(0, 180, 320 / 4, 240 - 180, ILI9341_BLACK);
		tft.drawRect(320 - 320 / 4, 180, 320 / 4, 240 - 180, ILI9341_BLACK);
		tft.drawRect(0, 180, 320, 240 - 180, ILI9341_BLACK);
		tft.drawRect(0, 0, 320, 100, ILI9341_BLACK);
		
		if ( touch() == 8 && AutoTX == 0)
		{
			AutoTX = 1;
			
			StatusMqtt == 0;
			do_send(&sendjob);
		}


		if (touch() == 5 && NextTX==1)
		{
			do_send(&sendjob); 
		}


		if (touch() == 6 || touch()==7 ) {
			
			
			if (SFx == 0) { SFx = 6; }
			SFx--;
			
		}
		tft.setCursor(135, 200);
		tft.setTextSize(3);
		String textSF[] = { "SF12","SF11","SF10","SF9 ","SF8 ","SF7 " };
		tft.print(textSF[SFx]);
		if (touch()==1)
		{
			resetFunc(); //call reset
		}
	}
	
}

void settings_display()
{

}

int touch()


{

	int ret = -1;
	if (!ts.touched()) { return ret; }
	TS_Point p = ts.getPoint();
	p.x = p.x - 300;
	p.y = p.y - 300;
	ret = 1;
	if (p.x > 875) { ret = 2; }
	if (p.x > 1750) { ret = 3; }
	if (p.x > 2625) { ret = 4; }

	if (p.y < 1750) {
		ret += 4;
	}
	return ret;
}

#pragma endregion

#pragma region WiFi




void setup_wifi() {
	delay(10);
	
	tft.print("Connecting to ");
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);
	tft.println(ssid);
	WiFi.setHostname(espHostname); //set ESP hostname
	WiFi.mode(WIFI_STA); // set Wifi mode
	WiFi.begin(ssid, password);
	byte versuche = 10;
	while (WiFi.status() != WL_CONNECTED && versuche>0) {
		delay(500);
		Serial.print(".");
		tft.print(".");
		versuche--;
	}

	if (versuche == 0)
	{
		tft.println("");
		tft.print("Connecting to ");
		Serial.println();
		Serial.print("Connecting to ");
		Serial.println(ssid1);
		tft.println(ssid1);
		
		delay(1000);
		WiFi.begin(ssid1, password1);
	
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
		tft.print(".");
		
	}
	}
	

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
	tft.println("");
	tft.println("WiFi connected");
	tft.println("IP address: ");
	tft.println(WiFi.localIP());
	
}
#pragma endregion



//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#pragma region GPS
#define RXD2 16
#define TXD2 17



bool gpsAlive = false;
uint8_t coords[6]; // 6*8 bits = 48

void GpsDaten()
{
	
	if (fuGPS.read())
	{
		
		
		
		gpsAlive = true;
		
		

		if (fuGPS.hasFix() == true)
		{
			gps_alt = millis();
			Serial.println(String(fuGPS.Latitude, 6) + "," + String(fuGPS.Longitude, 6));
			
			int32_t lat = fuGPS.Latitude * 10000;
			int32_t lon = fuGPS.Longitude * 10000;

			// Pad 2 int32_t to 6 8uint_t, skipping the last byte (x >> 24)
			coords[0] = lat;
			coords[1] = lat >> 8;
			coords[2] = lat >> 16;

			coords[3] = lon;
			coords[4] = lon >> 8;
			coords[5] = lon >> 16;
			Serial.print(coords[0], HEX);
			Serial.print(coords[1], HEX);
			Serial.print(coords[2], HEX);
			Serial.print(coords[3], HEX);
			Serial.print(coords[4], HEX);
			Serial.println(coords[5], HEX);
		}
	}

}
#pragma endregion

#pragma region Mqtt


void callback(char* topic, byte* payload, unsigned int length) {

	Serial.print("Message arrived [");
	Serial.print(topic);
	Serial.print("] ");
	String color("#");
	String command;
	byte Zaehler = 0;
	
	for (int i = 0; i < length; i++) {

		if (String((char)payload[i]) == ",")
		{

			Zaehler++;
			i++;
		}

		if (String((char)payload[i]) != "\"" & String((char)payload[i]) != "{" & String((char)payload[i]) != "}") {
			mqttvalue[Zaehler] += String((char)payload[i]);
			;
		}
	}
	for (int i = 0; i < 60; i++) {
		Serial.print(i);
		Serial.print("     ");
		Serial.println(mqttvalue[i]);
	}
	Serial.println();

	


	if (command.toInt() > 0) {

	}

}

void reconnect() {
	// Loop until we're reconnected
	while (!client.connected()) {
		
		Serial.print("Attempting MQTT connection...");
		tft.setTextSize(1);
		tft.setCursor(0, 0);
		tft.fillScreen(ILI9341_RED);
		tft.print("Attempting MQTT connection...");
		// Attempt to connect
		if (client.connect("Lora", mqttUser, mqttPassword)) {
			Serial.println("connected");
			tft.fillScreen(ILI9341_GREEN);
			tft.print("connected");
			// Once connected, publish an announcement...

			// ... and resubscribe
			client.subscribe("+/devices/+/up");
		}
		else {
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println(" try again in 5 seconds");
			// Wait 5 seconds before retrying
			delay(5000);
		}
		tft.fillScreen(ILI9341_BLACK);
	}
}



#pragma endregion

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#pragma region LoRaWan





// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui(u1_t* buf) { }
void os_getDevEui(u1_t* buf) { }
void os_getDevKey(u1_t* buf) { }

static uint8_t mydata[] = "He";

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).


// Pin mapping
const lmic_pinmap lmic_pins = {
	.nss = 2,
	.rxtx = LMIC_UNUSED_PIN,
	.rst = 5,
	.dio = {4,15},
};

void onEvent(ev_t ev) {
	Serial.print(os_getTime());
	Serial.print(": ");
	switch (ev) {
	case EV_SCAN_TIMEOUT:
		Serial.println(F("EV_SCAN_TIMEOUT"));
		break;
	case EV_BEACON_FOUND:
		Serial.println(F("EV_BEACON_FOUND"));
		break;
	case EV_BEACON_MISSED:
		Serial.println(F("EV_BEACON_MISSED"));
		break;
	case EV_BEACON_TRACKED:
		Serial.println(F("EV_BEACON_TRACKED"));
		break;
	case EV_JOINING:
		Serial.println(F("EV_JOINING"));
		break;
	case EV_JOINED:
		Serial.println(F("EV_JOINED"));
		break;
	case EV_RFU1:
		Serial.println(F("EV_RFU1"));
		break;
	case EV_JOIN_FAILED:
		Serial.println(F("EV_JOIN_FAILED"));
		break;
	case EV_REJOIN_FAILED:
		Serial.println(F("EV_REJOIN_FAILED"));
		break;
	case EV_TXCOMPLETE:
		Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
		cue = 0;
		delay(2000);
		NextSendTime = millis() + DelayTime[SFxLast];
		if (alt_MQTT_TIME != mqttvalue[16])
		{
			mqtt_alt = millis();
			StatusMqtt = 1;
			mqtt_alt = millis();
		}
		else
		{
			//keine neuen Mqtt Daten
			StatusMqtt = 2;
		}
		
		

		
		
		if (LMIC.txrxFlags & TXRX_ACK)
			Serial.println(F("Received ack"));
		if (LMIC.dataLen) {
			Serial.println(F("Received "));
			Serial.println(LMIC.dataLen);
			Serial.println(F(" bytes of payload"));
		}
		// Schedule next transmission
		os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
		break;
	case EV_LOST_TSYNC:
		Serial.println(F("EV_LOST_TSYNC"));
		break;
	case EV_RESET:
		Serial.println(F("EV_RESET"));
		break;
	case EV_RXCOMPLETE:
		// data received in ping slot
		Serial.println(F("EV_RXCOMPLETE"));
		break;
	case EV_LINK_DEAD:
		Serial.println(F("EV_LINK_DEAD"));
		break;
	case EV_LINK_ALIVE:
		Serial.println(F("EV_LINK_ALIVE"));
		break;
	default:
		Serial.println(F("Unknown event"));
		break;
	}
}

void do_send(osjob_t* j) {
	// Check if there is not a current TX/RX job running
	for (int i = 0; i < 60; i++) {
		mqttvalue[i] = "";
	}
	LMIC_setDrTxpow(SFx, 14);
	SFxLast = SFx;

	if (LMIC.opmode & OP_TXRXPEND) {
		Serial.println(F("OP_TXRXPEND, not sending"));
	}
	else {
		// Prepare upstream data transmission at the next possible time.

		
		


		LMIC_setTxData2(1, coords, sizeof(coords), 0);
		LMIC.txend = 0;
		
		Serial.println(F("Packet queued"));
		cue = 1;
		StatusMqtt = 0;
		

	}
	// Next TX is scheduled after TX_COMPLETE event.
}
#pragma endregion

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------






void setup() {
	Serial.begin(115200);
	Serial5.begin(9600,SERIAL_8N1, RXD2, TXD2);
	tft.begin();
	tft.setRotation(3);
	tft.fillScreen(ILI9341_BLACK);
	tft.setTextColor(ILI9341_WHITE);
	tft.setTextSize(2);
	tft.println("Starting");
	
	
	
	setup_wifi();

	// MQTT connect
	Serial.print("setup MQTT...");
	tft.print("setup MQTT...");
	delay(500);
	// connecting to the mqtt server
	client.setServer(mqtt_server, 1883);
	client.setCallback(callback);
	Serial.println("done!");
	tft.println("done!");
	ts.begin();
	
	
	Serial.print("6");
	
	
	while (!Serial && (millis() <= 1000))
		;
	Serial.print("7");
#ifdef VCC_ENABLE
	// For Pinoccio Scout boards
	pinMode(VCC_ENABLE, OUTPUT);
	digitalWrite(VCC_ENABLE, HIGH);
	delay(1000);
#endif
	Serial.print("8");
	// LMIC init
	delay(1000);
	os_init();
	// Reset the MAC state. Session and pending data transfers will be discarded.
	Serial.print("9");
	LMIC_reset();
	Serial.print("10");
	// Set static session parameters. Instead of dynamically establishing a session
	// by joining the network, precomputed session parameters are be provided.
#ifdef PROGMEM
// On AVR, these values are stored in flash and only copied to RAM
// once. Copy them to a temporary buffer here, LMIC_setSession will
// copy them into a buffer of its own again.
	uint8_t appskey[sizeof(APPSKEY)];
	uint8_t nwkskey[sizeof(NWKSKEY)];
	memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
	memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
	LMIC_setSession(0x1, DEVADDR, nwkskey, appskey);
#else
// If not running an AVR with PROGMEM, just use the arrays directly
	LMIC_setSession(0x1, DEVADDR, NWKSKEY, APPSKEY);
#endif

#if defined(CFG_eu868)
	// Set up the channels used by the Things Network, which corresponds
	// to the defaults of most gateways. Without this, only three base
	// channels from the LoRaWAN specification are used, which certainly
	// works, so it is good for debugging, but can overload those
	// frequencies, so be sure to configure the full frequency range of
	// your network here (unless your network autoconfigures them).
	// Setting up channels should happen after LMIC_setSession, as that
	// configures the minimal channel set.
	// NA-US channels 0-71 are configured automatically
	LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
	LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
	LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
	LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
	LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
	LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
	LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
	LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
	LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK, DR_FSK), BAND_MILLI);      // g2-band
	// TTN defines an additional channel at 869.525Mhz using SF9 for class B
	// devices' ping slots. LMIC does not have an easy way to define set this
	// frequency and support for class B is spotty and untested, so this
	// frequency is not configured here.
#elif defined(CFG_us915)
	// NA-US channels 0-71 are configured automatically
	// but only one group of 8 should (a subband) should be active
	// TTN recommends the second sub band, 1 in a zero based count.
	// https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
	LMIC_selectSubBand(1);
#endif

	// Disable link check validation
	LMIC_setLinkCheckMode(0);

	// TTN uses SF9 for its RX2 window.
	LMIC.dn2Dr = DR_SF9;

	// Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
	LMIC_setDrTxpow(SFx, 14);

	// Start job
	delay(1000);
	tft.fillScreen(ILI9341_BLACK);
	do_send(&sendjob);
}

void loop() {
	developer1 = 1;
	if (!client.connected()) {
		delay(100);
		reconnect();
		byte M_FIX = 99;
		byte M_nextTX = 99;
		byte M_Mqtt = 99;
	}
	
	client.loop();

	os_runloop_once();

	GpsDaten();

	Display();
	


	
}